
const tabla = document.querySelector('#expe')
let containerHTML = '';
function obtenerDatos() {
    fetch('http://127.0.0.1:5501/datos.json')
        .then(Response => Response.json())
        .then(datos => {
            for (const dataex of datos.experiencia) {
                containerHTML += `
                <tr>
                    <td>${dataex.empresa}</td>
                    <td>${dataex.descripcion}</td>
                    <td>${dataex.duracion}</td>
                    <td>${dataex.cargo}</td>
                </tr>
                `
                tabla.innerHTML = containerHTML
            }

            if (resumir) {
                resumir.nombre.innerHTML = datos.nombre + " " + datos.apellido
                resumir.edad.innerHTML = datos.edad
            }

        });
}

const div = document.querySelector('#sobremi');
let containerHTML1 = '';
function obtenerInfo() {
    fetch('http://127.0.0.1:5501/datos.json')
        .then(Response => Response.json())
        .then(datos => {
            datos.sobremi.forEach(sobremi => {
                const row = document.createElement('tr');
                row.innerHTML += `
                    <div>${sobremi.universidad}</div>
                    <div>${sobremi.habilidades}</div>
                    <div>${sobremi.nacimiento}</div>
                `;
                div.appendChild(row);
            });

        });
}

const divs = document.querySelector('#estudios');
let containerHTML2 = '';
function obtenerEstudio() {
    fetch('http://127.0.0.1:5501/datos.json')
        .then(Response => Response.json())
        .then(datos => {
            datos.estudios.forEach(estudios => {
                const rows = document.createElement('tr');
                rows.innerHTML += `
                    <div>${estudios.semestre}</div>
                    <div>${estudios.titulacion}</div>
                `;
                divs.appendChild(rows);
            });

        });
}

const bloque = document.querySelector('#contacto');
let containerHTML3 = '';
function obtenerContacto() {
    fetch('http://127.0.0.1:5501/datos.json')
        .then(Response => Response.json())
        .then(datos => {
            datos.contacto.forEach(contacto => {
                const rows = document.createElement('tr');
                rows.innerHTML += `
                    <div>${contacto.url}</div>
                    <div>${contacto.correo}</div>
                    <div>${contacto.residencia}</div>
                `;
                bloque.appendChild(rows);
            });

        });
}

const bloque1 = document.querySelector('#uni');
let containerHTM3 = '';
function obtenerUni() {
    fetch('http://127.0.0.1:5501/datos.json')
        .then(Response => Response.json())
        .then(datos => {
            datos.uni.forEach(uni => {
                const rows = document.createElement('tr');
                rows.innerHTML += `
                    <div>${uni.programa}</div>
                    <div>${uni.nombre}</div>
                    <div>${uni.semestre}</div>
                `;
                bloque1.appendChild(rows);
            });

        });
}

const bloque2 = document.querySelector('#nombre');
let containerHTML4 = '';
function obtenerNombre() {
    fetch('http://127.0.0.1:5501/datos.json')
        .then(Response => Response.json())
        .then(datos => {
            datos.nombre.forEach(nombre => {
                const rows = document.createElement('tr');
                rows.innerHTML += `
                    <div>${datos.nombre}</div>
                `;
                bloque2.appendChild(rows);
            });

        });
}

obtenerNombre();
obtenerUni();
obtenerInfo();
obtenerEstudio();
obtenerContacto();
obtenerDatos();